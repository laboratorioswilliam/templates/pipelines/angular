#!/bin/bash
COUNT_LINHAS=$(cat tmp-proxy.txt | wc -l)
for i in $(seq 1 $COUNT_LINHAS)
do
   LINE_INFO=$(sed "${i}q;d" tmp-proxy.txt)
   sed -i 's|'"$(echo '$'"$LINE_INFO" | rev | cut -d_ -f2- | rev | xargs)"'|'"$(echo $LINE_INFO | cut -d ':' -f 2-3 | xargs)"'|' dist/openshift/cm-template-web.yml
done
