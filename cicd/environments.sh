#!/bin/bash
SIGLA=$1
AMBIENTE=$2
AMBIENTE=$(echo $AMBIENTE | cut -d / -f1)

SIGLA_APP=$(echo "${SIGLA}_")

## PEGA AS VARIAVEIS DO PROJETO DENTRO DO GITLAB
touch tmp.txt
env | grep "$SIGLA_APP" | grep $AMBIENTE >> tmp.txt
sed -i 's|=|: |' tmp.txt
sed -i -e 's/^/  /' tmp.txt
cat tmp.txt

## PEGAR AS VAREVEIS DO PROJETO DO REFERECIADA A PROXY
touch tmp-proxy.txt
env | grep "$SIGLA_APP" | grep PROXY | grep $AMBIENTE >> tmp-proxy.txt
sed -i 's|=|: |' tmp-proxy.txt

cat tmp-proxy.txt

cat tmp.txt >> openshift/cm-template-web.yml
