#!/bin/bash
REFERENCIA_BRANCH=$(echo "$CI_BUILD_REF_NAME" | tr '[:lower:]' '[:upper:]')
REFERENCIA_BRANCH=$(echo $REFERENCIA_BRANCH | cut -d / -f1)
URL_AUDITORIA=$(eval echo '$'"URL_AUDITORIA_${REFERENCIA_BRANCH}")
URL_SCAADM_SERVICE=$(eval echo '$'"URL_SCAADM_SERVICE_${REFERENCIA_BRANCH}")
URL_SCA_SERVICE=$(eval echo '$'"URL_SCA_SERVICE_${REFERENCIA_BRANCH}")
URL_SUITE=$(eval echo '$'"URL_SUITE_${REFERENCIA_BRANCH}")
URL_WSO2=$(eval echo '$'"URL_WSO2_${REFERENCIA_BRANCH}")

ROUTER_API=$(eval echo '$'"ROUTER_API_${REFERENCIA_BRANCH}")

NOME_CONFIGMAP=$(echo "cm-${CI_PROJECT_NAME}")

sed -i 's|$URL_AUDITORIA|'"$URL_AUDITORIA"'|' src/environments/environment.prod.ts
sed -i 's|$URL_SCAADM_SERVICE|'"$URL_SCAADM_SERVICE"'|' src/environments/environment.prod.ts
sed -i 's|$URL_SCA_SERVICE|'"$URL_SCA_SERVICE"'|' src/environments/environment.prod.ts
sed -i 's|$URL_SUITE|'"$URL_SUITE"'|' src/environments/environment.prod.ts

sed -i 's|cm-template-web|'"$NOME_CONFIGMAP"'|' openshift/cm-template-web.yml

sed -i 's|$URL_AUDITORIA|'"$URL_AUDITORIA"'|' openshift/cm-template-web.yml
sed -i 's|$URL_SCAADM_SERVICE|'"$URL_SCAADM_SERVICE"'|' openshift/cm-template-web.yml
sed -i 's|$URL_SCA_SERVICE|'"$URL_SCA_SERVICE"'|' openshift/cm-template-web.yml
sed -i 's|$URL_SUITE|'"$URL_SUITE"'|' openshift/cm-template-web.yml
sed -i 's|$URL_WSO2|'"$URL_WSO2"'|' openshift/cm-template-web.yml

sed -i 's|$ROUTER_API|'"$ROUTER_API"'|' openshift/cm-template-web.yml
