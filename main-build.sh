#!/bin/bash

SONAR_URL=$1

rm -f *.conf Dockerfile 2>/dev/null
wget -q $PIPILINE_URL_BASE/cicd/ci-cd.sh
chmod +x ./ci-cd.sh && ./ci-cd.sh
SIGLA=$(echo $CI_PROJECT_NAME | cut -d '-' -f 1 | tr '[:lower:]' '[:upper:]')
AMBIENTE=$(echo $CI_BUILD_REF_NAME | tr '[:lower:]' '[:upper:]')
wget -q $PIPILINE_URL_BASE/cicd/environments.sh
chmod +x environments.sh && ./environments.sh $SIGLA $AMBIENTE
mkdir -p compilados
npm run build-prod
mv dist/* compilados/
wget -q $PIPILINE_URL_BASE/nginx/nginx.sh
wget -q $PIPILINE_URL_BASE/nginx/nginx.conf
wget -q $PIPILINE_URL_BASE/nginx/default.conf
wget -q $PIPILINE_URL_BASE/nginx/proxy.conf
wget -q $PIPILINE_URL_BASE/Dockerfile
if [ -e openshift/entryPoint.sh ]; then cp -p openshift/entryPoint.sh ./ ; else wget -q  $PIPILINE_URL_BASE/nginx/entryPoint.sh ; fi
mv entryPoint.sh nginx.sh nginx.conf default.conf proxy.conf Dockerfile dist/
mv openshift/ compilados/ dist/
wget -q $PIPILINE_URL_BASE/cicd/config-map.sh
chmod +x config-map.sh && ./config-map.sh
