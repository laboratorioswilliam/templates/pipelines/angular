#!/bin/bash

BRANCH=$1
BRANCH=$(echo $CI_BUILD_REF_NAME | cut -d / -f1)
TAG_FRONT=$(echo $CI_BUILD_REF_NAME | cut -d / -f2)

if [ "$BRANCH" = "homolog" ] && [ $TAG_FRONT ]
then
  sed -i 's|$TAG_FRONT|'"$TAG_FRONT"'|' src/environments/environment.prod.ts
elif [ "$BRANCH" = "prod" ] && [ $TAG_FRONT ]
then
  sed -i 's|$TAG_FRONT|'"$TAG_FRONT"'|' src/environments/environment.prod.ts
else
  sed -i 's|$TAG_FRONT|'"$TAG_FRONT"'|' src/environments/environment.prod.ts
fi
