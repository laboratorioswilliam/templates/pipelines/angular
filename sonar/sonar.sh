SONAR_URL=$1
SONAR_USER=$2
SONAR_PASSWORD=$3

sed -i 's|'SONAR_URL'|'$SONAR_URL'|' sonar.js 
sed -i 's|'SONAR_USER'|'$SONAR_USER'|' sonar.js 
sed -i 's|'SONAR_PASSWORD'|'$SONAR_PASSWORD'|' sonar.js 

npm install -g --silent sonarqube-scanner && \
mkdir -p /home/jenkins/.sonar/native-sonar-scanner/ && \
wget -q http://nexus.okd.infraestrutura.gov.br/repository/files/sonar/sonar-scanner-cli-4.5.0.2216-linux.zip && \
unzip -qq sonar-scanner-cli-4.5.0.2216-linux.zip && \
mv sonar-scanner-4.5.0.2216-linux /home/jenkins/.sonar/native-sonar-scanner/ && \

export NODE_PATH=/home/jenkins/.npm-global/lib/node_modules
node ./sonar.js

