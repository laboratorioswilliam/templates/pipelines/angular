// eslint-disable-next-line @typescript-eslint/no-var-requires
const scanner = require("sonarqube-scanner");

scanner(
  {
    serverUrl: "SONAR_URL",
    options: {
      "sonar.login": "SONAR_USER",
      "sonar.password": "SONAR_PASSWORD",
    },
  },
  () => process.exit()
);
